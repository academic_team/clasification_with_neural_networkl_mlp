#!/usr/bin/env python
# coding: utf-8


get_ipython().run_line_magic('matplotlib', 'inline')


# 
# # Diagnóstico Cancer de Mama usando uma Rede Neural MLP
# 
# Este é um exemplo do uso da biblioteca scikit-learn para treinar uma rede neural MLP e classificar pacientes com tumores malignos ou benignos.
# 
# A base possui a seguinte distribuição:
# 
# - 212 pacientes da classe MALIGNO
# - 357 pacientes da classe BENINGNO
# 
# São 569 pacientes ao todo e 30 atributos descrevendo as características de cada paciente.

print(__doc__)

# Importando matplotlib 
import matplotlib.pyplot as plt

# Importando conjunto de dados, classificadores e métricas de desempenho
from sklearn import datasets, metrics
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier

import numpy as np
import pandas as pd


# ## Base de Dados de Cancer
# 
# Vamos listas os nomes dos 30 atributos da base:

# In[134]:


from sklearn.datasets import load_breast_cancer
data = load_breast_cancer(as_frame=True)


print("Atributos: ", data.feature_names)
print(" ")
print("Classes: ", data.target_names)


# # Analisando Atributos (Histograma)

# In[135]:


fig = plt.figure(figsize = (15,20))
ax = fig.gca()
x = data.data.hist(ax = ax)


# # Treinando o Modelo de Classificação
# 
# Agora criamos a rede MLP com a estrutura desejada e definimos seus hiperparâmetros de treinamento. Estes parâmetros são importantes pois irão definir a qualidade do resultado.
# 
# Podemos dividir os dados em subconjuntos de treinamento e teste e ajustar uma rede neural MLP (Multilayer Perceptron). 
# O classificador após o ajuste poderá ser usado para prever a classe das amostras do subconjunto de teste.
# 
# A rede MLP possui 30 unidades de entrada, um número de camadas ocultas e neurônios em cada camada oculta definido pelo usuário e 1 neurônio na camada de saída.

# In[136]: Alterando os parametros de número das camadas ocultas para 100,60,30.

# Creando uma rede MLP
# Definindo a rede:

# hidden_layer_sizes: tupla, tamanho = n_camadas - 2, default=(100,)
# alpha: float, default=0.0001
# learning_rate: {‘constant’, ‘invscaling’, ‘adaptive’}, default=’constant’
# learning_rate_init: double, default=0.001
# max_iter: int, default=200
# early_stopping: bool, default=False
# tol: float, default=1e-4

#In[138]: Alterando a taxa de aprendizagem para atualização de peso em "adaptativo"
clf = MLPClassifier(solver='lbfgs', alpha=0.0001, random_state=1, max_iter=2000,
            early_stopping=True, hidden_layer_sizes=(100,60,30), learning_rate='adaptive')

#In[139]: trabalhando com um conjunto de treino 0.3
# Dividindo o conjundo de Dados
X_train, X_test, y_train, y_test = train_test_split(data.data, data.target, test_size=0.3, random_state=42)

# Treinar o modelo com o conjunto de treinamento 
clf.fit(X_train, y_train)

# Prever a classe do dígito com o subconjunto de teste
predicted = clf.predict(X_test)


# # Testando o Classificador
# 
# Exibindo o relatório de classificação da rede MLP para o subconjunto de teste. 
# 

# In[137]:


print(f"Relatório de Classificação do {clf}:\n"
      f"{metrics.classification_report(y_test, predicted)}\n")


# # Matriz de Confusão do Classificador
# 
# Exibindo a matriz `confusion matrix <confusion_matrix>` dos valores reais e dos valores previstos. As linhas correspondem aos valores reais e as colunas aos valores previstos. A contagem de acertos em relação ao subconjunto dedados de teste mostrará na diagonal principal os acertos e nas demais posições os erros.
# 

# In[138]:


disp = metrics.plot_confusion_matrix(clf, X_test, y_test)
disp.figure_.suptitle("Confusion Matrix")
print(f"Matriz de Confusão:\n{disp.confusion_matrix}")

plt.show()


# # O Trabalho Prático 2
# 
# O objetivo deste trabalho será gerar a melhor rede neural possível, testando as combinações máximas deparâmetros de treinamento que permitam melhorar o desempenho do classificar ao final da tarefa.
# 
# Para isso o grupo deverá definir o roteiro de experimentos para organizar toda essa combinação. Dentre as possibilidade de experimentos estão:
# 
# * Modificar método de divisão do conjunto de dados.
# * Modificar métodos de treinamento de rede MLP e os parâmetros de cada método de treinamento.
# * Modificar a estruturada física rede MLP, testanto número de camadas e de neurônios, além de diferentes funções de ativação.
# 
# Há várias outras classes parâmetros além dessas que citei acima para serem investigados.
# 
# Data de Entrega: 23/04/2021
# Grupo é o mesmo do seminário!
# 
# Relatório PDF com todos experimentos programados e realizados, resultados obtidos em cada caso e a configuração que melhor foi obtida.
# 
# O relatório pode ser um próprio notebook Jupyter, ou um outro documento com todos os gráficos do python.
# 
#In[]:



